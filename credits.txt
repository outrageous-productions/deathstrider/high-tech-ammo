bmaps // Pillowblaster
models // It's literally just a sphere
shaders // Kom
sounds/AcidBurningTarget.wav  // Killing Floor
sounds/AcidRocketGoop1.wav // UT2k4
sounds/AcidRocketGoop2.wav // UT2k4
sounds/BurstShotFire.ogg // Quake 3, slightly modified
sounds/LightningRodLoop.ogg // Enemy Territory: Quake Wars
sounds/LightningRodPopOff.ogg // Quake 4
sounds/LightningRodPrism.wav // Red Alert 2
sounds/SlowBulletEffect.wav // Warcraft 3, slightly modified
sounds/WeakenBulletEffect.wav // Warcraft 3, slightly modified
sprites/debris // Derived from DeathStrider
sprites/Goop // ???
sprites/Pickups // Pillow
sprites // Lightning effects from ???
zscript // Kom, with some effects and adjustments by PillowBlaster, and advice from Accensus
