uniform float timer;

Material ProcessMaterial()
{
	Material material;
	vec3 normal = getTexel(gl_TexCoord[0].st).xyz * 2.0 - 1.0; // Global normals.
	vec3 absNormal = abs(normal); // Absolute value of normals.
	vec2 offset = vec2(sin(timer), cos(timer)); // A rotating offset based on the timer.
	// Create a rolling factor based on the timer.
	vec3 factor = vec3(
		max(abs(mod(timer * 3.0 + 0.0, 3.0) - 1.5) -0.5, 0.0),
		max(abs(mod(timer * 3.0 + 1.0, 3.0) - 1.5) -0.5, 0.0),
		max(abs(mod(timer * 3.0 + 2.0, 3.0) - 1.5) -0.5, 0.0)
	);
	// Generate a '3d noise texture' from the global normals and the offset.
	vec3 noise = texture(perlin, normal.xy + offset).rgb * absNormal.z
		+ texture(perlin, normal.xz + offset).rgb * absNormal.y
		+ texture(perlin, normal.yz + offset).rgb * absNormal.x;
	noise *= 0.15; // Scale noise to a small amount for use as distortion.
	// Generate a second 3d noise texture by using the distortion.
	vec3 base = texture(perlin, normal.xy + noise.xy).rgb * absNormal.z
		+ texture(perlin, normal.xz + noise.xz).rgb * absNormal.y
		+ texture(perlin, normal.yz + noise.yz).rgb * absNormal.x;
	base /= absNormal.x + absNormal.y + absNormal.z; // Normalize the brightness.
	base *= factor; // Adjust values based on the timer factor.
	material.Base = vec4(vec3(1.0), pow(1.0 - abs(base.r + base.g + base.b - 0.5), 16.0)); // Use a power to condense brightness of the generated texture at the 50% mark.
	material.Base *= vec4(noise.r, 2.0 * noise.g + 0.4, 3.0 * noise.b + 0.6, 1.0); // Modulate once more to generate a color.
	return material;
}

vec4 ProcessLight(Material material, vec4 color)
{
	return color * material.Base;
}
