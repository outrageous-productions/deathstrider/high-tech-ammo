class OPP_12GaBurst : DSP_Buckshot
{
	const BLAST_RANGE = DSCONST_ONEMETER * 14.0;
	const BLAST_OFFSET = DSCONST_ONEMETER * 2.0;
	const BLAST_HALF_ANGLE = 40.0;
	const BLAST_DOT = sin(90.0 - BLAST_HALF_ANGLE);

	override void PostBeginPlay()
	{
		OnExplode(DSProjectile.EType_Geometry);
		Destroy();
	}

	override void OnExplode(int type)
	{
		A_StartSound("BurstShot/Fire", 66, 0, 1.0);
		A_StartSound("BurstShot/Fire", 66, CHANF_OVERLAP, 1.0);
		Vector3 forward = (cos(Angle) * cos(Pitch), sin(Angle) * cos(Pitch), -sin(Pitch));
		Vector3 up = (cos(Angle) * sin(Pitch), sin(Angle) * sin(Pitch), cos(Pitch));
		Vector3 right = (sin(Angle), -cos(Angle), 0.0);
		if(CountInv('MajesticElectrifier')) { ExtraDamageFactor += (0.4 * CountInv('MajesticElectrifier')); }
		for (int i = 0; i < 60; i++)
		{
			// Weight values more towards 1.0, less towards 0.0, because closer
			// values have less spread on the perpendicular axes, this is the
			// distance from the round that the particle will spawn.
			double spawnDistance = (1.0 - (frandom(0.0, 1.0) ** 2)) * BLAST_RANGE;
			// This rotates the perpendicular axis around the forward axis.
			double spawnRotation = frandom(0.0, 360.0);
			// This is an offset along the perpendicular axis, spread from the forward axis.
			double spawnAngle = frandom(0.0, BLAST_HALF_ANGLE);
			Vector3 particlePos = ((up * sin(spawnRotation) + right * cos(spawnRotation)) * sin(spawnAngle) + forward * cos(spawnAngle)) * spawnDistance;
			switch(random(1, 4))
			{
				case 1:
				case 2:
					Spawn('OPBurstSmoke', particlePos + Pos).Scale *= spawnDistance * 0.02;
					break;
				case 3:
					Spawn('OPBurstCinder', particlePos + Pos);
					break;
				case 4:
					Spawn('OPBurstBlast', particlePos + Pos).Scale *= spawnDistance * 0.02;
					break;
			}
		}
		SetOrigin(pos - forward * BLAST_OFFSET, false);
		Array<Actor> ThingsToBurst;
		BlockThingsIterator it = BlockThingsIterator.Create(self, BLAST_RANGE + BLAST_OFFSET);
		while (it.Next())
		{
			if (
				it.thing
				&& it.thing.bSHOOTABLE
				&& it.thing != self.target
				&& (deathmatch || !(it.thing is 'PlayerPawn'))
				&& (forward dot Vec3To(it.thing).Unit() > BLAST_DOT)
				&& (Distance3D(it.thing) < BLAST_RANGE + BLAST_OFFSET)
				&& target.CheckSight(it.thing, SF_IGNOREVISIBILITY | SF_IGNOREWATERBOUNDARY) // Check sight to the player because the firing cone starts behind the player and might otherwise miss when looking around a corner.
			)
			{
				ThingsToBurst.Push(it.thing);
				//it.thing.DamageMobj(self, target, random(30, 40), 'None');
			}
		}
		
		for(int t = 0; t < ThingsToBurst.Size(); t++)
		{
			ThingsToBurst[t].DamageMobj(self, target, int(round(max(100-(6*(ThingsToBurst.Size()-1)), 40) * ExtraDamageFactor)), 'None');
		}
	}

	Default
	{
		Speed 0;
		Mass 110;
		Damage 0;
	}
}


class OPBurstBlast : Actor
{
	Default
	{
		Renderstyle "Add";
		Scale 0.08;
		+NOINTERACTION
		+ROLLSPRITE
		+FORCEXYBILLBOARD
		+NOTIMEFREEZE
	}

	States
	{
		Spawn:
			TNT1 A 0 NoDelay
			{
				Vel = (frandom(-1.0, 1.0), frandom(-1.0, 1.0), frandom(-1.0, 1.0));
				A_SetScale(Scale.X + frandom(0.0, 0.15));
				A_SetRoll(frandom(0.0, 360.0));
			}
			HEXP ABCDEFG random(1, 2) Bright;
			Stop;
	}
}

class OPBurstSmoke : Actor
{
	Default
	{
		Renderstyle "Translucent";
		Alpha 0.0;
		Scale 0.05;
		+NOINTERACTION
		+ROLLSPRITE
		+INTERPOLATEANGLES
		+FORCEXYBILLBOARD
		+NOTIMEFREEZE
	}

	double rollRate;

	States
	{
		Spawn:
			TNT1 A 0 NoDelay
			{
				A_SetScale(Scale.X + frandom(0.0, 0.1));
				A_SetRoll(frandom(0.0, 360.0));
				rollRate = frandom(1, -1);
			}
		PhaseOne:
			GNSM L 1
			{
				A_SetRoll(roll + rollRate, SPF_INTERPOLATE);
				A_FadeIn(0.1);
				return A_JumpIf(Alpha >= 0.5, "PhaseTwo");
			}
			Loop;
		PhaseTwo:
			GNSM L 1
			{
				A_SetRoll(roll + rollRate, SPF_INTERPOLATE);
				A_FadeOut(0.02);
				Scale += (0.01, 0.01);
			}
			Loop;
	}
}

class OPBurstCinder : Actor
{
	Default
	{
		Radius 2;
		Scale 0.02;
		Gravity 0.1;
		-NOGRAVITY
		+THRUACTORS
		+FORCEXYBILLBOARD
		+NOTIMEFREEZE
	}

	States
	{
		Spawn:
			TNT1 A 0 NoDelay
			{
				Vel = (frandom(-3.0, 3.0), frandom(-3.0, 3.0), frandom(2.0, 4.0));
				A_SetScale(Scale.X + frandom(0.0, 0.02));
			}
			BSFP A 1 Bright
			{
				Vel += (frandom(-0.5, 0.5), frandom(-0.5, 0.5), frandom(-0.2, 0.2));
				Alpha = frandom(0.2, 1.0) - GetAge() * 0.02;
				Scale *= frandom(0.95, 0.98);
				return A_JumpIf(GetAge() > 60, 'Null');
			}
			Wait;
	}
}

class OPSpent12GaBurst : DSSpentBuckshot
{
	States
	{
		Spawn:
			EOPS A 0;
			Goto DSCasing::Spawn;
	}
}

class OP12GaBurstAmmo : DSBuckshotAmmo
{
	override double GetWeight(DSData data) { return 0.038; }
	override string GetIcon(DSData data) { return "OP12X0"; }
	override string GetTechnicalInfo(DSData data) { return "- Disperses a cloud of explosive particles\n\c[Orange]- Moderate damage\n\c[Green]- Hits all enemies in front of the shooter\n\c[Red]- Very limited range"; }
	override string GetLore(DSData data) { return "Within this sealed shell is a measure of high explosive nanoparticles that are designed to ignite moments after being expelled into the atmosphere. The result is a harmful wave of concussive force."; }
	override Color, string GetAmmoColor() { return 0xFF33EEFF, "Cyan"; }

	Default
	{
		Tag "12 Ga Burst";
		Inventory.Icon "OP12X0";
		DSAmmo.Projectile 'OPP_12GaBurst', 1;
		DSRoundAmmo.Casing 'OPSpent12GaBurst';
		DSItem.ModID "OutrageousProductions";
		DSAmmo.Stack 4, 12;
	}

	States
	{
		Spawn:
			OP12 A -1;
			OP12 B -1 A_SetScale(0.32);
			OP12 X -1 A_SetScale(0.3);
			Stop;
	}
}

class OP12GaBurstPack : DSAmmoSpawner
{
	Default
	{
		DSAmmoSpawner.Properties 'OP12GaBurstAmmo', 4;
	}
}

class OP12GaBurstBox : DSAmmoSpawner
{
	Default
	{
		DSAmmoSpawner.Properties 'OP12GaBurstAmmo', 12;
	}
}

class OP12GaBurstAmmo_Store : DSStoreItem
{
	override double GetCost() { return 1.30; }
	override double, int GetResupplyAmount() { return 12, 48; }
}