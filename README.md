# High Tech Ammo

An ammo addon for [Deathstrider](https://gitlab.com/accensi/deathstrider).

---

Provides a new 'high tech' variant of each of the core ammo types that perform some more specialized functions of their vanilla counterparts. These are generally rarer and more expensive than the base ammo as well. All of the ammo provided by this addon uses **cyan** as its HUD/magazine color.

## Ammunition Types

- **.50 AE Weakening** - Enemies hit by this round will both take more and deal less damage. The effect is more pronounced on weaker targets, but hitting a single target repeatedly will further enhance the effect.
- **12 Gauge Burst** - Deals damage to targets in a cone immediately in front of the player. This round cannot be used to hit targets at range.
- **.500 S&W Flechettes** - Fires five piercing flechettes in a wide, even pattern. Majestic charge will increase the penetration, but the damage remains the same.
- **5.56 Stun** - Applies a progressively stronger slowing effect to monsters hit, based on their mass. Like the 50AE, repeated hits will further slow a target. The effect has a maximum strength, but the duration continues to increase once at that cap.
- **7.62 Seeking** - Rounds that will arc around and adjust speed to hit targets. Not as powerful as a standard round.
- **Caustic Rocket Grenades** - Disperses caustic goop that coats enemies on impact and will further damage targets that contact it. In rocket mode, will leave goop behind as it travels. In grenade mode, throws goop in every direction.
- **50BMG Shock** - Deploys a shockrod that sticks to surfaces on impact and zaps random nearby targets. Triggers additional shocks on nearby targets on impact. While single-target damage is higher, damaging more targets will deal more total damage.
- **35mm Enrage** - Causes monsters in the area around the impact to attack random targets. Enemies will still attack you if there is nothing else to target. The duration of the effect is lessened on enemies with more health.

---

See [credits.txt](./credits.txt) for attributions.
